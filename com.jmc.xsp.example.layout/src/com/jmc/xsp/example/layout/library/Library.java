package com.jmc.xsp.example.layout.library;

import com.ibm.xsp.library.AbstractXspLibrary;

public class Library extends AbstractXspLibrary {

	public String getLibraryId() {
		return "com.jmc.xsp.example.layout.library";
	}
	public String getPluginId() {
		return "com.jmc.xsp.example.layout";
	}
	@Override
	public String[] getFacesConfigFiles() {
		return new String[]{
				"com/jmc/xsp/example/layout/config/headerComponent-faces-config.xml"
		};
	}
	@Override
	public String[] getXspConfigFiles() {
		return new String[]{
				"com/jmc/xsp/example/layout/config/headerComponent.xsp-config"
		};
	}
}