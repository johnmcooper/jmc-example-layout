package com.jmc.xsp.example.layout.renderer;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import com.ibm.xsp.renderkit.FacesRenderer;
import com.jmc.xsp.example.layout.component.HeaderComponent;

public class HeaderRenderer extends FacesRenderer {
	
	public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
		// nothing needed here as all rendering being done in encodeEnd
	}

	public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
		
		//check the component is the header component as expected
		if (!(component instanceof HeaderComponent)) {
			throw new IOException(
					"HeaderRenderer must be used with an instance of the Header Component");
		}
		
		HeaderComponent header = (HeaderComponent) component;
		ResponseWriter writer = context.getResponseWriter();
		
		// Write the header tag
		writer.startElement("header", header);

		// Add some content
		writer.startElement("h1", header);
		writer.write("Header Component");
		writer.endElement("h1");
		
		//close the header tag
		writer.endElement("header");
	}
}