package com.jmc.xsp.example.layout.component;

import javax.faces.component.UIOutput;

public class HeaderComponent extends UIOutput {
	private static final String DEFAULT_RENDERER_TYPE = "com.jmc.example.layout.header";
	private static final String DEFAULT_FAMILY = "com.jmc.example.layout";
	
	public HeaderComponent(){
		setRendererType(DEFAULT_RENDERER_TYPE);
	}
	
	@Override
	public String getFamily() {
		return DEFAULT_FAMILY;
		
	}
}
